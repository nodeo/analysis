<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests\Lexer;

use Parsing\Lexer\Input\InputQueueInterface;
use Parsing\Lexer\AbstractLexer;
use Parsing\Token\TokenType;

class MockLexer extends AbstractLexer
{
    public const T_VARIABLE = 1;
    public const T_EQUALS = 2;
    public const T_VALUE = 3;
    public const T_SEMICOLON = 4;

    /**
     * MockLexer constructor.
     *
     * @param $inputQueue InputQueueInterface
     */
    public function __construct(InputQueueInterface $inputQueue)
    {
        parent::__construct(
            $inputQueue,
            [
                self::T_VARIABLE => new TokenType(
                    self::T_VARIABLE,
                    'T_VARIABLE',
                    '\$([a-z][a-zA-Z0-9]*)'
                ),
                self::T_EQUALS => new TokenType(
                    self::T_EQUALS,
                    'T_EQUALS',
                    '='
                ),
                self::T_VALUE => new TokenType(
                    self::T_VALUE,
                    'T_VALUE',
                    '[0-9]+'
                ),
                self::T_SEMICOLON => new TokenType(
                    self::T_SEMICOLON,
                    'T_SEMICOLON',
                    ';'
                )
            ]
        );
    }
}
