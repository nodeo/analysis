<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests\Lexer;

use Parsing\Lexer\Input\InputQueueFactory;
use Parsing\Lexer\UndefinedTokenException;
use PHPUnit\Framework\TestCase;

class LexerTest extends TestCase
{
    public function testLexer()
    {
        $inputQueueFactory = new InputQueueFactory();
        $inputQueue = $inputQueueFactory->createQueue('$foo=1');

        $lexer = new MockLexer($inputQueue);
        $result = $lexer->next();

        self::assertEquals(
            MockLexer::T_VARIABLE,
            $result->getType()
                ->getId()
        );

        self::assertEquals('foo', $result->getValue());

        $result = $lexer->next();

        self::assertEquals(
            MockLexer::T_EQUALS,
            $result->getType()->getId()
        );

        self::assertEquals('=', $result->getValue());

        $result = $lexer->next();

        self::assertEquals(
            MockLexer::T_VALUE,
            $result->getType()->getId()
        );

        self::assertEquals('1', $result->getValue());

        $result = $lexer->next();

        self::assertEquals(
            MockLexer::T_END,
            $result->getType()->getId()
        );

        self::assertEquals(
            MockLexer::T_END,
            $result->getType()->getId()
        ); // Reached the end another time
    }

    public function testUnknownToken()
    {
        $this->expectException(UndefinedTokenException::class);
        $this->expectExceptionMessage('Undefined token "f"');

        $inputQueueFactory = new InputQueueFactory();
        $inputQueue = $inputQueueFactory->createQueue('foo=12');

        $lexer = new MockLexer($inputQueue);
        $lexer->next();
    }
}
