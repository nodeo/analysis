<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests\Parser;

use Parsing\Lexer\Input\InputQueueFactory;
use Parsing\Parser\UnexpectedTokenException;
use PHPUnit\Framework\TestCase;
use Tests\Lexer\MockLexer;

class ParserTest extends TestCase
{
    public function testParser()
    {
        $stream = new InputQueueFactory();
        $tokenStream = $stream->createQueue("\$a =  1;\n \$b\t=2 ;\$c=10");
        $tokenStream->rewind();

        $lexer = new MockLexer($tokenStream);
        $parser = new MockParser($lexer);

        self::assertEquals(
            [
                "a" => "1",
                "b" => "2",
                "c" => "10"
            ],
            $parser->parseVariables()
        );
    }

    public function testUnexpectedToken()
    {
        $this->expectException(UnexpectedTokenException::class);

        $stream = new InputQueueFactory();
        $tokenStream = $stream->createQueue('$a = 1$b=2;$c=10'); //
        // semicolon missed

        $lexer = new MockLexer($tokenStream);
        $parser = new MockParser($lexer);

        $parser->parseVariables();
    }
}
