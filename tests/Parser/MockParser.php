<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests\Parser;

use Parsing\Lexer\AbstractLexer;
use Parsing\Parser\AbstractParser;
use Tests\Lexer\MockLexer;

class MockParser extends AbstractParser
{
    public function parseVariables(): array
    {
        $variables = [];

        while (!$this->match(AbstractLexer::T_END)) {
            $name = $this->matchOrThrowException(MockLexer::T_VARIABLE);
            $this->updateLookahead(self::SKIP_WHITESPACES);

            $this->matchOrThrowException(MockLexer::T_EQUALS); // Consume equals
            $this->updateLookahead(self::SKIP_WHITESPACES);

            $value = $this->matchOrThrowException(MockLexer::T_VALUE);
            $this->updateLookahead(self::SKIP_WHITESPACES);

            $variables[$name] = $value;

            $this->matchOrThrowException(
                MockLexer::T_END,
                MockLexer::T_SEMICOLON
            );
            $this->updateLookahead(self::SKIP_WHITESPACES);
        }

        return $variables;
    }
}
