<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Lexer;

use JetBrains\PhpStorm\Pure;
use Parsing\Lexer\Input\InputQueueInterface;
use Parsing\Token\Token;
use Parsing\Token\TokenType;

/**
 * Class AbstractLexer
 * This class represents a lexical analyser.
 * It converts a sequence of characters from a {@link InputQueueInterface} into
 * a sequence of {@link Token}s.
 * This class is not instantiable as it is. It must be inherited from another
 * class which can optionally add new {@link TokenType}s to existent ones.
 *
 * @package Parsing\Lexer
 */
abstract class AbstractLexer
{
    public const T_END = -1;
    public const T_WHITESPACE = 0;

    private InputQueueInterface $input;
    private array $tokens;
    private ?string $lookahead;

    /**
     * AbstractLexer constructor.
     *
     * @param InputQueueInterface $inputQueue The token queue.
     * @param array $tokens The additional tokens to register, if any.
     */
    public function __construct(
        InputQueueInterface $inputQueue,
        array $tokens = []
    ) {
        $this->input = $inputQueue;

        if ($this->input->end()) {
            $this->input->rewind();
        }

        $this->tokens = $tokens + [
                self::T_END =>
                    new TokenType(
                        self::T_END,
                        'T_END',
                        '$' // End pattern (matched '')
                    ),
                self::T_WHITESPACE =>
                    new TokenType(
                        self::T_WHITESPACE,
                        'T_WHITESPACE',
                        '\s+'
                    )
            ];
    }

    /**
     * Consumes the next characters sequence and returns the token result.
     *
     * @return Token The token result.
     * @throws UndefinedTokenException If the current lookahead doesn't match
     * any of the registered token types.
     */
    public function next(): Token
    {
        $this->lookahead = $this->input->peek();

        $matchedTokens = $this->getMatchedTokens();

        if (empty($matchedTokens)) {
            throw new UndefinedTokenException(
                substr($this->lookahead, 0, 1)
            );
        }

        $matchedTokenId = array_key_first($matchedTokens); // Gets the first
        // token if several matched

        $readSequence = $matchedTokens[$matchedTokenId][0]; // Actual read
        // sequence

        $this->input->forward(strlen($readSequence));
        $this->input->rewind();

        return $this->getMatchedToken($matchedTokens, $matchedTokenId);
    }

    private function getMatchedTokens(): array
    {
        $matchedTokens = [];

        foreach ($this->tokens as $token) {
            if ($this->match($token, $matches)) {
                $matchedTokens[$token->getId()] = $matches;
            }
        }

        return $matchedTokens;
    }

    #[Pure] private function getMatchedToken(
        array $matchedTokens,
        int $matchedTokenId
    ): Token {
        $matchedTokenType = $this->tokens[$matchedTokenId];

        $tokenValue = $matchedTokens[$matchedTokenId][array_key_last(
            $matchedTokens[$matchedTokenId]
        )];

        return new Token(
            $matchedTokenType,
            $tokenValue
        );
    }

    /**
     * Checks if the the current lookahead matches the specified token type.
     *
     * @param TokenType $token The token type.
     * @param null &$matches The matches array.
     * @return bool <code>true</code> if the current lookahead matches the
     * specified token type, <code>false</code> otherwise.
     */
    private function match(TokenType $token, &$matches): bool
    {
        return preg_match($token->getPattern(), $this->lookahead, $matches);
    }

    #[Pure] public function getTokenName(int $token): string
    {
        return $this->tokens[$token]->getName();
    }
}
