<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Lexer\Input;

use IO\Stream\StreamInterface;

class InputQueue implements InputQueueInterface
{
    private StreamInterface $stream;
    private int $cursor;

    public function __construct(StreamInterface $stream)
    {
        $this->stream = $stream;
        $this->cursor = 0;
    }

    public function rewind()
    {
        $this->stream->seek($this->cursor);
    }

    public function end(): bool
    {
        return $this->stream->eof() ||
            $this->stream->tell() === $this->stream->getSize();
    }

    public function forward(int $n)
    {
        $this->cursor += $n;
    }

    public function peek(): string
    {
        return $this->stream->read();
    }
}
