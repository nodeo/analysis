<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Lexer\Input;

interface InputQueueInterface
{
    public function end(): bool;

    public function rewind();

    public function forward(int $n);

    public function peek(): string;
}
