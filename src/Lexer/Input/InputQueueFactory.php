<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Lexer\Input;

use IO\Stream\StreamFactory;
use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\Pure;

class InputQueueFactory
{
    public function createQueue(string $content): InputQueue
    {
        $streamFactory = new StreamFactory();
        $stream = $streamFactory->createStream($content);

        return $this->createQueueFromStream($stream);
    }

    public function createQueueFromFile(
        string $filename,
        string $mode = 'r'
    ): InputQueue {
        $streamFactory = new StreamFactory();
        $stream = $streamFactory->createStreamFromFile($filename, $mode);

        return $this->createQueueFromStream($stream);
    }

    public function createQueueFromResource(
        $resource
    ): InputQueue {
        $streamFactory = new StreamFactory();
        $stream = $streamFactory->createStreamFromResource($resource);

        return $this->createQueueFromStream($stream);
    }

    #[Pure] public function createQueueFromStream(
        StreamInterface $stream
    ): InputQueue {
        return new InputQueue($stream);
    }
}
