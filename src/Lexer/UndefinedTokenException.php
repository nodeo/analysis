<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Lexer;

use JetBrains\PhpStorm\Pure;
use Parsing\Token\TokenType;
use RuntimeException;

/**
 * Class UndefinedTokenException
 *
 * @package Parsing\Lexer
 *
 * This exception is thrown when the current lookahead doesn't match any of
 * the {@link TokenType}s registered in the current lexer.
 */
class UndefinedTokenException extends RuntimeException
{
    /**
     * UndefinedTokenException constructor.
     *
     * @param string $lookahead The current lookahead.
     */
    #[Pure] public function __construct(string $lookahead)
    {
        parent::__construct(sprintf('Undefined token "%s"', $lookahead));
    }
}
