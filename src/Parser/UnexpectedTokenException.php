<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Parser;

use Parsing\Lexer\AbstractLexer;
use Parsing\Token\Token;
use RuntimeException;

class UnexpectedTokenException extends RuntimeException
{
    /**
     * UnexpectedTokenException constructor.
     */
    public function __construct(
        AbstractLexer $lexer,
        array $expectedTokens,
        Token $lookahead
    ) {
        if (count($expectedTokens) === 1) {
            $message = $lexer->getTokenName($expectedTokens[0]);
        } else {
            $message = 'one of the following [';
            $message .= implode(
                ',',
                array_map(
                    function ($tokenId) use ($lexer) {
                        return $lexer->getTokenName($tokenId);
                    },
                    $expectedTokens
                )
            );
            $message .= ']';
        }

        parent::__construct(
            sprintf(
                "The matched token is not the expected one.\n" .
                    "\tWaiting for %s and received %s.",
                $message,
                $lookahead->getType()->getName()
            )
        );
    }
}
