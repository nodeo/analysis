<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Parser;

use JetBrains\PhpStorm\ExpectedValues;
use JetBrains\PhpStorm\Pure;
use Parsing\Lexer\AbstractLexer;
use Parsing\Token\Token;

/**
 * Class AbstractParser
 *
 * @package Parsing\Parser
 */
abstract class AbstractParser
{
    public const KEEP_WHITESPACES = 0;
    public const SKIP_WHITESPACES = 1;

    protected AbstractLexer $input;
    protected Token $lookahead;

    public function __construct(AbstractLexer $input)
    {
        $this->input = $input;
        $this->updateLookahead();
    }

    #[ExpectedValues([self::KEEP_WHITESPACES, self::SKIP_WHITESPACES])]
    protected function updateLookahead(
        int $option = self::KEEP_WHITESPACES
    ) {
        $this->lookahead = $this->input->next();

        if (
            $option === self::SKIP_WHITESPACES &&
            $this->lookahead->getType()->getId() === AbstractLexer::T_WHITESPACE
        ) {
            $this->lookahead = $this->input->next();
        }
    }

    protected function matchOrThrowException(int ...$tokens): string
    {
        if (!$this->match(...$tokens)) {
            throw new UnexpectedTokenException(
                $this->input,
                $tokens,
                $this->lookahead
            );
        }

        return $this->lookahead->getValue();
    }

    #[Pure] protected function match(int ...$tokens): bool
    {
        return in_array($this->lookahead->getType()->getId(), $tokens);
    }
}
