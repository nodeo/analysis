<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Token;

class TokenType
{
    private int $id;
    private string $name;
    private string $pattern;

    public function __construct(int $id, string $name, string $pattern)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pattern = sprintf('/^%s/', $pattern);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }
}
