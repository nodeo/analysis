<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Parsing\Token;

class Token
{
    private TokenType $type;
    private string $value;

    /**
     * Token constructor.
     *
     * @param TokenType $type The token type.
     * @param string $value The token value.
     */
    public function __construct(TokenType $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * Returns the token type.
     *
     * @return TokenType The token type.
     */
    public function getType(): TokenType
    {
        return $this->type;
    }

    /**
     * Returns the token value.
     *
     * @return string The token value.
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
